# What factors make a Young Adult author more likely to be successful? 
## Author: Jessica Marx

### The Dataset
- The dataset is made up of 100 YA titles published in the year 2016. 
- The definition of a `high-rated` title is a rating of 4.5 or higher __and__ 100 or more reviews.
- Of the 100 titles in this dataset, `27%` were `high-rated`.

### Recommendations for Young Authors: 
- The greater challenge is to garner enough reviews; it is easier to achieve a rating of `+4.5` (`62%`) than to get `+100` reviews (`41%`).
- Length of the book plays a factor: the sweet spot is between 300-400 pages. 
- Publishers matter; if a title was highly-rated, it was most likely to be published by Delacourte. 
- Focus on landing a publisher that has a history of high number of reviews for YA books; if number of reviews weren't a factor in achieving a high-rating, Amazon would be the recommended publisher, but Amazon books tend to not get enough reviews to be considered highly-rated. 
- Books that were part of a series were all highly-rated. There are considerations associated with this (if an author's debut novel was successful enough to warrant a sequel, it's more likely that it will be well-received). Still, series books are obviously popular with Young Adult readers, so a Young Adult author would do well to consider how a debut novel might develop into a series. 
- Finally, know that it's not easy to get a highly-rated novel (basically 1 in 4 YA novels achieve this); if you don't succeed this time, try again! 

### Please find the analysis behind these recommendations, including data visualizations <a href="https://gitlab.com/jfmarx/ya-authors/-/blob/master/ya-authors-ratings-analysis.ipynb">HERE</a>.

